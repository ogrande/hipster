package br.com.beardhipster.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.app.beardhipster.R;
import br.com.beardhipster.HttpManager.RequestPackage;
import br.com.beardhipster.model.User;
import br.com.beardhipster.task.MatchLikedTask;
import br.com.beardhipster.util.SessionManager;
import br.com.beardhipster.util.Util;

public class ViewProfileActivity extends AppCompatActivity {

    private TextView titleTextView;
    private ImageView coverImageView;
    private ImageView likeImageView;

    private SessionManager manager;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        this.manager = new SessionManager(this);

        titleTextView  = (TextView) findViewById(R.id.name);
        coverImageView = (ImageView) findViewById(R.id.coverImageView);
        likeImageView  = (ImageView) findViewById(R.id.likeImageView);

        user = (User) getIntent().getSerializableExtra("user");

        if (user != null){
            titleTextView.setText(user.getName());
            likeImageView.setTag(R.drawable.ic_like);
            likeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = (int) likeImageView.getTag();
                    if (id == R.drawable.ic_like) {
                        likeImageView.setTag(R.drawable.ic_liked);
                        likeImageView.setImageResource(R.drawable.ic_liked);
                        user_match(Util.URL_MATCH,user);
                    } else {
                        likeImageView.setTag(R.drawable.ic_like);
                        likeImageView.setImageResource(R.drawable.ic_like);
                        user_match(Util.URL_UNMATCH,user);
                    }
                }
            });


            if (user.getUrlImagem() != null) {
                Picasso.with(this)
                        .load(user.getUrlImagem())
                        .into(coverImageView);
            } else {
                coverImageView.setImageResource(R.drawable.beard_1);
            }
            coverImageView.setTag(user.getName());
        }
    }

    public void user_match(String URL, User userliked){
        String idUser = manager.getUid();
        RequestPackage p = new RequestPackage();
        p.setUri(URL);
        p.setMethod("POST");
        p.setParam("id1", idUser);
        p.setParam("id2", String.valueOf(userliked.getId()));
        MatchLikedTask loginTask = new MatchLikedTask(this);
        loginTask.execute(p);
    }


}
