package br.com.beardhipster.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import br.com.app.beardhipster.R;
import br.com.beardhipster.HttpManager.RequestPackage;
import br.com.beardhipster.task.MainTask;
import br.com.beardhipster.util.SessionManager;
import br.com.beardhipster.util.Util;


public class MainActivity extends AppCompatActivity {

    private static final int REQ_EDIT = 100;
    protected SessionManager manager;
    protected Context ctx;

    private String content = null;

    private MenuItem menuPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        manager = new SessionManager(this);
        ctx = this;
        if (!manager.isLoggedIn()) {
            callLoginActivity();
        }

        initUsers();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_settings, menu);
        menuPreferences = menu.findItem(R.menu.menu_settings);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_preferences) {
            Intent it = new Intent(MainActivity.this, EditProfileActivity.class);
            it.putExtra("user_id",manager.getUid());
            startActivity(it);
        }
        return super.onOptionsItemSelected(item);
    }

    private void callLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void initUsers(){
        RequestPackage p = new RequestPackage();
        p.setUri(Util.URL_USERS);
        p.setMethod("GET");
        p.setParam("user_id",manager.getUid());
        MainTask mainTask = new MainTask(this);
        mainTask.execute(p);
    }

}