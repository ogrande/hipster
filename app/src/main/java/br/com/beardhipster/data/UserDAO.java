package br.com.beardhipster.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.beardhipster.model.User;

public class UserDAO {

    private static UserDAO instance;

    private SQLiteDatabase db;

    public static UserDAO getInstance(Context context) {
        if (instance == null) {
            instance = new UserDAO(context.getApplicationContext());
        }
        return instance;
    }

    private UserDAO(Context context) {
        DBHelper dbHelper = DBHelper.getInstance(context);
        db = dbHelper.getWritableDatabase();
    }

    public List<User> list() {

        String[] columns = {
                UserContract.Columns._ID,
                UserContract.Columns.NOME,
                UserContract.Columns.EMAIL,
        };

        List<User> users = new ArrayList<>();

        try (Cursor c = db.query(UserContract.TABLE_NAME, columns, null, null, null, null, UserContract.Columns.NOME)) {
            if (c.moveToFirst()) {
                do {
                    User p = UserDAO.fromCursor(c);
                    users.add(p);
                } while (c.moveToNext());
            }

            return users;
        }

    }

    private static User fromCursor(Cursor c) {
        long id = c.getLong(c.getColumnIndex(UserContract.Columns._ID));
        String nome = c.getString(c.getColumnIndex(UserContract.Columns.NOME));
        String email = c.getString(c.getColumnIndex(UserContract.Columns.EMAIL));
        return new User(id, nome, email);
    }

    public void save(User user) {
        ContentValues values = new ContentValues();
        values.put(UserContract.Columns.NOME, user.getName());
        values.put(UserContract.Columns.EMAIL, user.getEmail());
        long id = db.insert(UserContract.TABLE_NAME, null, values);
        user.setId(id);
    }

    public void update(User user) {
        ContentValues values = new ContentValues();
        values.put(UserContract.Columns.NOME, user.getName());
        values.put(UserContract.Columns.EMAIL, user.getEmail());
        db.update(UserContract.TABLE_NAME, values, UserContract.Columns._ID + " = ?", new String[]{String.valueOf(user.getId())});
    }

    public void delete(User user) {
        db.delete(UserContract.TABLE_NAME, UserContract.Columns._ID + " = ?", new String[]{String.valueOf(user.getId())});
    }
}
