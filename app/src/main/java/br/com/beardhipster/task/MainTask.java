package br.com.beardhipster.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import br.com.app.beardhipster.R;
import br.com.beardhipster.HttpManager.HttpManager;
import br.com.beardhipster.HttpManager.RequestPackage;
import br.com.beardhipster.Parser.UserJsonParser;
import br.com.beardhipster.adapter.UserAdapter;
import br.com.beardhipster.model.User;

/**
 * Created by cassiopaixao on 01/05/2017.
 */


public class MainTask extends AsyncTask<RequestPackage, Void, List<User>> {

    String content = "";
    private Activity activity;
    private RecyclerView MyRecyclerView;
    private RecyclerView.Adapter adapter;

    public MainTask(Context context) {
        activity = (Activity) context;
    }

    private void updateDisplay(List<User> users) {
        MyRecyclerView = (RecyclerView) activity.findViewById(R.id.cardView);
        MyRecyclerView.setHasFixedSize(true);
        MyRecyclerView.setLayoutManager(new LinearLayoutManager(activity));

        if (users.size() > 0 & MyRecyclerView != null) {
            adapter = new UserAdapter(activity, users);
            MyRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    protected List<User> doInBackground(RequestPackage... params) {
        content = HttpManager.getData(params[0]);
        return UserJsonParser.getUsers(content);
    }

    @Override
    protected void onPostExecute(List<User> users) {
        updateDisplay(users);
    }
}
