package br.com.beardhipster.model;

/**
 * Created by cassiopaixao on 30/04/2017.
 */

public class CallbackJson {

    private boolean error;
    private String message;


    public String getMessage() {
        return message;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
