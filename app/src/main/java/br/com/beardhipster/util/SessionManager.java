package br.com.beardhipster.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import br.com.beardhipster.model.User;

public class SessionManager {
    private static String TAG = SessionManager.class.getSimpleName();

    private User user;
    SharedPreferences pref;

    Editor editor;
    Context _context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "bearhipster";

    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_UID = "uid";
    private static String KEY_USER;

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);

        editor.commit();

        Log.d(TAG, "Usuario Logado modificado na sessão!");
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }


    //recupera o uid vindo do web service para fazer resetar a senha
    public void setUid(String uid){
        editor.putString(KEY_UID, uid);
        editor.commit();
    }

    public String getUid(){
        return pref.getString(KEY_UID, null );
    }

}
