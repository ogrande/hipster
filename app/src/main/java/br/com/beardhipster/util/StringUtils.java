package br.com.beardhipster.util;

import android.widget.EditText;
/**
 * Created by jsramos on 06/05/2017.
 */

public class StringUtils {
    public static boolean isValid(String str){
        return str!=null && !str.trim().equals("");
    }

    public static boolean isValid(EditText edt){
        if (edt != null)
            return isValid(edt.getText().toString());
        return false;
    }

    public static StringBuilder incrementText(StringBuilder sb, String text, String separator){
        if (!isValid(separator))
            separator = "|";
        if (sb == null)
            sb = new StringBuilder();
        if (isValid(text))
            sb.append(text);
        sb.append(separator);

        return sb;
    }
}
