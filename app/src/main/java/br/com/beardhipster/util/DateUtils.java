package br.com.beardhipster.util;

import android.widget.EditText;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jsramos on 06/05/2017.
 */

public class DateUtils {
    public static Date parseEditDataToDate(EditText edt, String mask) {
        if (edt != null && mask != null && !mask.equals("")) {
            try {
                ParsePosition pos = new ParsePosition(0);
                SimpleDateFormat sdf = new SimpleDateFormat(mask);
                if (StringUtils.isValid(edt.getText().toString())) {
                    Date date = sdf.parse(edt.getText().toString(), pos);
                    return date;
                }
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public static Date parseEditDataToDate(EditText edt){
        return parseEditDataToDate(edt,"dd/MM/yyyy");
    }

    public static Date parseStringToDate(String str){
        try {
            ParsePosition pos = new ParsePosition(0);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if (StringUtils.isValid(str)) {
                Date date = sdf.parse(str, pos);
                return date;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

        return null;

    }

}
